import { MemberPipe } from './../pipes/member/member.pipe';
import { RightMenuBarComponent } from './right-menu-bar/right-menu-bar.component';
import { HeaderComponent } from './header/header.component';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout.component';
import { LayoutRoutingModule } from './layout.routing';
import { MyFilesComponent } from './my-files/my-files.component';
import { SideBarMenuComponent } from './side-bar-menu/side-bar-menu.component';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material';
import { SharingComponent } from './sharing/sharing.component';
import { MatMenuModule } from '@angular/material/menu';
import { FileService } from '../providers/file/file.service';
import { FileStore } from '../states/file.state';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCardModule } from '@angular/material/card';
import { DialogDeleteComponent } from './my-files/dialog-delete/dialog-delete';
import { FileDropDirective, FileSelectDirective } from 'ng2-file-upload';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { AutoCompletedComponent } from './header/auto-completed/auto-completed.component';
import { NavigationComponent } from './header/navigation/navigation.component';
import { SearchComponent } from './search/search.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: true
};

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        MatCheckboxModule,
        LayoutRoutingModule,
        MatMenuModule,
        MatDialogModule,
        MatCardModule,
        MatProgressBarModule,
        PerfectScrollbarModule
    ],
    exports: [],
    declarations: [
        SideBarMenuComponent,
        MyFilesComponent,
        LayoutComponent,
        HeaderComponent,
        RightMenuBarComponent,
        MemberPipe,
        SharingComponent,
        DialogDeleteComponent,
        FileDropDirective,
        FileSelectDirective,
        AutoCompletedComponent,
        NavigationComponent,
        SearchComponent
    ],
    entryComponents: [
        DialogDeleteComponent
    ],
    providers: [FileService, FileStore, {
        provide: PERFECT_SCROLLBAR_CONFIG,
        useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }],
})
export class LayoutModule { }
