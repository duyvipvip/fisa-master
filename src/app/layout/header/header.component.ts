import { FileService } from './../../providers/file/file.service';
import { IFile } from './../../models/IFile';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MenuPositionX } from '@angular/material/menu/typings/menu-positions';
import { FileStore } from '../../states/file.state';
import { debounce } from 'lodash';
import { Router } from '@angular/router';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  routes: IFile[];
  keyword: string;
  results: IFile[] = [];
  constructor(
    private router: Router,
    private fileStore: FileStore,
    private fileService: FileService) {
  }

  ngOnInit() {
    this.fileStore.route.subscribe(folders => {
      this.routes = folders;
    });
  }

  onChange(searchString) {
    const changed = debounce(() => {
      this.fileService.getFileInFolder(searchString).subscribe(folders => {
        this.results = folders;
      });
    }, 400);

    changed();
  }

  onLogout() {
    this.router.navigate(['/login']);
  }
}
