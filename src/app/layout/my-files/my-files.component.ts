import { DialogDeleteComponent } from './dialog-delete/dialog-delete';
import { IFile, FileType } from './../../models/IFile';
import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { remove, isEmpty } from 'lodash';
import { FileStore } from '../../states/file.state';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FileUploader, FileItem } from 'ng2-file-upload';
@Component({
    selector: 'app-my-files',
    templateUrl: './my-files.component.html',
    styleUrls: ['./my-files.component.scss']
})
export class MyFilesComponent implements OnInit, OnDestroy {
    @ViewChild('inputFolder') private inputFolderElementRef: ElementRef;
    @ViewChild('inputEdit') private inputEditElementRef: ElementRef;
    selectedFiles: IFile[] = [];
    ischeckAll: Boolean = false;
    files: IFile[] = [];
    newFolder: IFile;
    editFile: IFile;
    private sub: any;
    id: string;
    fileUploading: FileItem;
    searchMode: Boolean = false;
    constructor(private fileStore: FileStore,
        public dialog: MatDialog,
        private route: ActivatedRoute,
        private router: Router) { }

    ngOnInit() {
        this.fileStore.files.subscribe(files => {
            this.files = files;
        });

        this.sub = this.route.params.subscribe(params => {
            this.id = params['id'] || 'root';
            if (!this.router.url.includes('/search')) {
                this.fileStore.loadFile('', this.id);
            }
        });

        this.route.queryParams.subscribe(params => {
            if (this.router.url.includes('/search')) {
                const keyword = params['keyword'] || '';
                this.fileStore.loadFile(keyword);
            }
        });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    checkAll() {
        this.files = this.files.map(f => {
            f.checked = !this.ischeckAll;
            return f;
        });
    }

    onChangeRow(file: IFile) {
        this.selectedFiles = this.files.filter(f => f.checked);
    }
    createNewFolder() {
        this.newFolder = {
            id: '-1',
            type: FileType.folder,
            parent: this.id
        };
        setTimeout(() => {
            this.inputFolderElementRef.nativeElement.focus();
        }, 100);
    }

    onEnter(event) {
        // tslint:disable-next-line:no-debugger
        debugger;
        if (this.newFolder) {
            this.inputFolderElementRef.nativeElement.blur();
        } else {
            this.inputEditElementRef.nativeElement.blur();
        }
    }

    onBlur(file: IFile) {
        this.fileStore.addFile(file);
        this.newFolder = undefined;
    }

    rename(file: IFile) {
        this.editFile = Object.assign({}, file);
        file.isEdit = true;
    }

    onRename(file: IFile) {
        const currentFile = this.files.find(f => f.id === file.id);
        currentFile.isEdit = false;
        this.fileStore.renameFile(file);
    }

    delete(file: IFile) {
        const dialogRef = this.dialog.open(DialogDeleteComponent, {
            width: '500px',
            data: file
        });

        dialogRef.afterClosed().subscribe((result: IFile) => {
            if (result.isDeleted) {
                this.fileStore.removeFile(result);
            }
        });
    }

    view(file: IFile) {
        if (file.type === FileType.folder) {
            this.router.navigate(['home/' + file.id]);
        }
    }

    uploadFile(file: IFile) {
    }

    download(file: IFile) {
        this.fileStore.downloadFile(file);
    }
}
