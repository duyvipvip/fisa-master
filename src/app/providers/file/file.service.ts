import { environment } from './../../../environments/environment.prod';
import { IFile, FileType } from './../../models/IFile';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { HttpClient } from '@angular/common/http';
import { RequestOptions, ResponseContentType, Headers } from '@angular/http';
import { HttpResponse } from '@angular/common/http/src/response';
import { Http } from '@angular/http';
@Injectable()
export class FileService {

    constructor(private http: HttpClient, private http2: Http) {
    }

    getFileInFolder(keyword: string = '', parentId: string = ''): Observable<IFile[]> {
        const body = {
            search: keyword,
            parentId: parentId
        };
        return this.http.post('/api/file/find', body).map((data: any[]) => {
            data.map(f => {
                f.type = f['isFile'] ? f.format : FileType.folder;
                f.id = f['_id'];
                return f as IFile;
            });
            return data;
        });
    }

    createFolder(folder: IFile): Observable<any> {
        const body = {
            name: folder.name,
            parentId: folder.parent || 'root'
        };
        return this.http.post('/api/file/folder', body);
    }

    updateFile(file: IFile) {
        const body = {
            name: file.name
        };
        return this.http.put('/api/file/update/' + file.id, body);
    }

    getRoute(id: string): Observable<IFile[]> {
        if (!id || id === 'root') {
            return new Observable(observer => {
                observer.next([]);
            });
        }
        return this.http.get('/api/file/parent/' + id).map(data => data as IFile[]);
    }

    downloadFile(id: string): Observable<Blob> {
        const options = new RequestOptions({ responseType: ResponseContentType.Blob });
        return this.http2.get(`${environment.apiUrl}/api/file/download/` + id, options)
            .map(response => response.blob());
    }

    deleleFile(id: string) {
        return this.http.delete('/api/file/delete/' + id);
    }
}
