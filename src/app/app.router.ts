import { RegisterComponent } from './register/register.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { LayoutComponent } from './layout/layout.component';
import { LoginComponent } from './login/login.component';
import { RouterModule, Route, ExtraOptions } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Route[] = [
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent},
    { path: 'admin', loadChildren: 'app/admin/admin.module#AdminModule' },
    { path: '', loadChildren: 'app/layout/layout.module#LayoutModule' },
    { path: '**', component: PageNotFoundComponent }
];

const config: ExtraOptions = {
    useHash: false,
};

@NgModule({
    imports: [RouterModule.forRoot(routes, config)],
    exports: [RouterModule],
})
export class AppRoutingModule {
}
