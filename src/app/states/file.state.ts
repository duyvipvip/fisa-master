import { ToastsManager } from 'ng2-toastr/src/toast-manager';
import { Injectable } from '@angular/core';
import { FileService } from '../providers/file/file.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { IFile, FileType } from '../models/IFile';
import { Observable } from 'rxjs/Observable';
import { saveAs as importedSaveAs } from 'file-saver';

@Injectable()
export class FileStore {
    private _files: BehaviorSubject<Array<IFile>> = new BehaviorSubject(new Array());
    private _route: BehaviorSubject<IFile[]> = new BehaviorSubject([{ id: '-1', name: 'JTC Share' }]);

    get route() {
        return this._route.asObservable();
    }
    get files() {
        return this._files.asObservable();
    }
    constructor(private fileService: FileService, private toastr: ToastsManager) {
    }

    loadFile(keyword: string = '', parent: string = '', folder?: IFile) {
        this.fileService.getRoute(parent).subscribe((files: any[]) => {
            files.map(f => {
                f.type = FileType.folder;
                f.id = f['_id'];
                return f as IFile;
            });
            files.unshift({ id: '', name: 'JTC Share' });
            this._route.next(files);
        });
        this.fileService.getFileInFolder(keyword, parent).subscribe(((files: any[]) => {
            files.map(f => {
                f.type = f['isFile'] ? f.format : FileType.folder;
                f.id = f['_id'];
                return f as IFile;
            });
            this._files.next(files);
        }));
    }

    addFile(file: IFile) {
        if (!file.name) {
            this.toastr.error('Please provide a name for the new folder.');
            return;
        }
        // call create folder api
        if (file.type === FileType.folder) {
            this.fileService.createFolder(file).subscribe((folder) => {
                folder.id = folder._id;
                folder.type = FileType.folder;
                this._files.getValue().unshift(folder);
                return this._files.next(this._files.getValue());
            }, (err) => {
                this.toastr.error(err);
            });
        } else {
            this._files.getValue().unshift(file);
            return this._files.next(this._files.getValue());
        }
    }

    renameFile(file: IFile) {
        if (!file.name) {
            return;
        }
        this.fileService.updateFile(file).subscribe(() => {
            const files = this._files.getValue();
            const newFile = files.find(f => f.id === file.id);
            newFile.name = file.name;
            this._files.next(files);
        }, err => {
            this.toastr.error('Something wrong, please try again');
        });
    }

    removeFile(file: IFile) {
        if (!file.name) {
            return;
        }
        this.fileService.deleleFile(file.id).subscribe(() => {
            const files = this._files.getValue();
            const index = files.findIndex(f => f.id === file.id);
            files.splice(index, 1);
            this._files.next(files);
        });
    }

    downloadFile(file: IFile) {
        this.fileService.downloadFile(file.id).subscribe((blob: Blob) => {
            importedSaveAs(blob, file.name);
        });
    }


}
