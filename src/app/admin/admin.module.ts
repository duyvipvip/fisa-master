import { RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard/dashboard.component';

@NgModule({
    imports: [RouterModule.forChild([
        {path: '', component: AdminComponent, children: [
            { path: 'dashboard', component: DashboardComponent},
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
        ]},
    ])],
    exports: [],
    declarations: [AdminComponent, DashboardComponent],
    providers: [],
})
export class AdminModule { }
